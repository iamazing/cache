/**
 * @Title: JRedisClusterServiceImpl.java
 * @Package com.beaconem.iot.broker.service.redis.impl
 * @Description: TODO(用一句话描述该文件做什么)
 * @author hb8588
 * @date 2016年9月26日 上午9:23:33
 * @version V1.0
 */
package com.ideal.manage.cache.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.ideal.manage.cache.config.CommonConstants;
import com.ideal.manage.cache.service.JRedisService;
import com.ideal.manage.cache.tool.SerializeUtil;
import org.apache.commons.lang3.StringUtils;
import redis.clients.jedis.JedisCluster;
import redis.clients.jedis.Tuple;
import redis.clients.jedis.exceptions.JedisConnectionException;
import redis.clients.jedis.exceptions.JedisDataException;
import redis.clients.jedis.exceptions.JedisException;

import java.util.*;

/**
 * @ClassName: JRedisClusterServiceImpl
 * @Description: TODO(这里用一句话描述这个类的作用)
 * @author hb8588
 * @date 2016年9月26日 上午9:23:33
 *
 */
public class JRedisClusterServiceImpl extends BaseService implements JRedisService {

	/**
	 * @Title: @Description: TODO(这里用一句话描述这个方法的作用) @author:hb @return @throws
	 */
	public JRedisClusterServiceImpl() {
		super();
	}

	/**
	 * @Title: @Description: TODO(这里用一句话描述这个方法的作用) @author:hb @param localAddress @param port @param jedisClusterClient @return @throws
	 */
	public JRedisClusterServiceImpl(String port, JedisClusterClient jedisClusterClient) {
		super();
		this.port = port;
		this.jedisClusterClient = jedisClusterClient;
	}

	/**
	 *
	 * @Title: init;
	 * @Description: 初始化服务信息
	 * @param ;
	 * @return void; @throws;
	 */
	public void init() {
		JedisCluster jedisCluster = jedisClusterClient.getJedisCluster();
		if (jedisCluster.hexists(CommonConstants.SERVER_REDIS_HSET_NAME, this.getLocalIpPort())) {
			setServerId(jedisCluster.hget(CommonConstants.SERVER_REDIS_HSET_NAME, this.getLocalIpPort()));
		} else {
			long number = jedisCluster.incr(CommonConstants.SERVER_REDIS_HSET_NAME_INCR);
			jedisCluster.hset(CommonConstants.SERVER_REDIS_HSET_NAME, this.getLocalIpPort(), String.valueOf(number));
			setServerId(String.valueOf(number));
		}
	}

	/**
	 *
	 * @Title: getServerList;
	 * @Description: 获取服务IP地址列表;
	 * @param @param deviceId
	 * @param @return;
	 * @return Map<String,String>; @throws;
	 */
	public Map<String, String> getServerList() {

		JedisCluster jedisCluster = jedisClusterClient.getJedisCluster();
		return jedisCluster.hgetAll(CommonConstants.SERVER_REDIS_HSET_NAME);
	}

	@Override
	public boolean add(String key, Object value) {
		JedisCluster jedisCluster = null;
		String str = "false";
		try {
			jedisCluster = jedisClusterClient.getJedisCluster();
			str = jedisCluster.set(key.getBytes(), SerializeUtil.serialize(value));
		} catch (JedisException e) {
			logger.error("redis error:{}", e);
			handleJedisException(e);
			throw e;
		}
		return "OK".equals(str) ? true : false;
	}

	@Override
	public boolean addnx(String key, String value, int expiry) {
		JedisCluster jedisCluster = null;
		long retVal = -1;
		try {
			jedisCluster = jedisClusterClient.getJedisCluster();
			retVal = jedisCluster.setnx(key, value);
			jedisCluster.expire(key, expiry);
		} catch (JedisException e) {
			logger.error("redis error:{}", e);
			handleJedisException(e);
			throw e;
		}
		return retVal > 0;
	}

	@Override
	public boolean addString(String key, String value) {
		JedisCluster jedisCluster = null;
		String str = "false";
		try {
			jedisCluster = jedisClusterClient.getJedisCluster();
			str = jedisCluster.set(key, value);
		} catch (JedisException e) {
			logger.error("redis error:{}", e);
			handleJedisException(e);
			throw e;
		}
		return "OK".equals(str) ? true : false;
	}

	@Override
	public boolean addString(String key, String value, long expiry) {
		JedisCluster jedisCluster = null;
		String str = "false";
		try {
			jedisCluster = jedisClusterClient.getJedisCluster();
			str = jedisCluster.setex(key, Integer.parseInt(expiry + ""), value);
		} catch (JedisException e) {
			logger.error("redis error:{}", e);
			handleJedisException(e);
			throw e;
		}
		return "OK".equals(str) ? true : false;
	}

	@Override
	public boolean exists(String key) {
		JedisCluster jedisCluster = null;
		boolean retval = false;
		try {
			jedisCluster = jedisClusterClient.getJedisCluster();
			retval = jedisCluster.exists(key);
		} catch (JedisException e) {
			logger.error("redis error:{}", e);
			handleJedisException(e);
			throw e;
		}
		return retval;
	}

	@Override
	public boolean addHash(String key, Map<String, String> hash) {
		JedisCluster jedisCluster = null;
		String str = "false";
		try {
			jedisCluster = jedisClusterClient.getJedisCluster();
			str = jedisCluster.hmset(key, hash);
		} catch (JedisException e) {
			logger.error("redis error:{}", e);
			handleJedisException(e);
			throw e;
		}
		return "OK".equals(str) ? true : false;
	}

	@Override
	public Long delHashAndExp(String key, int expiry, String... field) {
		JedisCluster jedisCluster = null;
		try {
			jedisCluster = jedisClusterClient.getJedisCluster();
			if (jedisCluster.hdel(key, field) >= 0) {
				return jedisCluster.expire(key, expiry);
			}
			return 0l;
		} catch (JedisException e) {
			logger.error("redis error:{}", e);
			handleJedisException(e);
			throw e;
		}
	}

	@Override
	public Long delHash(String key, String... field) {
		JedisCluster jedisCluster = null;
		try {
			jedisCluster = jedisClusterClient.getJedisCluster();
			return jedisCluster.hdel(key, field);
		} catch (JedisException e) {
			logger.error("redis error:{}", e);
			handleJedisException(e);
			throw e;
		}
	}

	@Override
	public Long del(String key) {
		JedisCluster jedisCluster = null;
		try {
			jedisCluster = jedisClusterClient.getJedisCluster();
			return jedisCluster.del(key.getBytes());
		} catch (JedisException e) {
			logger.error("redis error:{}", e);
			handleJedisException(e);
			throw e;
		}
	}

	@Override
	public Long incr(String key) {
		JedisCluster jedisCluster = null;
		try {
			jedisCluster = jedisClusterClient.getJedisCluster();
			return jedisCluster.incr(key.getBytes());
		} catch (JedisException e) {
			logger.error("redis error:{}", e);
			handleJedisException(e);
			throw e;
		}
	}

	@Override
	public Long incrBy(String key, long value) {
		JedisCluster jedisCluster = null;
		try {
			jedisCluster = jedisClusterClient.getJedisCluster();
			return jedisCluster.incrBy(key, value);
		} catch (JedisException e) {
			logger.error("redis error:{}", e);
			handleJedisException(e);
			throw e;
		}
	}

	@Override
	public Long expire(String key, int seconds) {
		JedisCluster jedisCluster = null;
		try {
			jedisCluster = jedisClusterClient.getJedisCluster();
			return jedisCluster.expire(key, seconds);
		} catch (JedisException e) {
			logger.error("redis error:{}", e);
			handleJedisException(e);
			throw e;
		}
	}

	@Override
	public Long ttl(String key) {
		JedisCluster jedisCluster = null;
		try {
			jedisCluster = jedisClusterClient.getJedisCluster();
			return jedisCluster.ttl(key);
		} catch (JedisException e) {
			logger.error("redis error:{}", e);
			handleJedisException(e);
			throw e;
		}
	}

	@Override
	public Long expireAndTTL(String key, int seconds) {
		JedisCluster jedisCluster = null;
		try {
			jedisCluster = jedisClusterClient.getJedisCluster();
			long ttl = jedisCluster.ttl(key);
			if (ttl > 0) {
				return ttl;
			}
			return jedisCluster.expire(key, seconds);
		} catch (JedisException e) {
			logger.error("redis error:{}", e);
			handleJedisException(e);
			throw e;
		}
	}

	@Override
	public boolean add(String key, Object value, long expiry) {
		JedisCluster jedisCluster = null;
		String str = "false";
		try {
			jedisCluster = jedisClusterClient.getJedisCluster();
			str = jedisCluster.set(key.getBytes(), SerializeUtil.serialize(value), "NX".getBytes(), "EX".getBytes(), expiry);
		} catch (JedisException e) {
			logger.error("redis error:{}", e);
			handleJedisException(e);
			throw e;
		}
		return "OK".equals(str) ? true : false;
	}

	@Override
	public boolean add(String key, String value, long expiry) {
		JedisCluster jedisCluster = null;
		String str = "false";
		try {
			jedisCluster = jedisClusterClient.getJedisCluster();
			str = jedisCluster.set(key, value, "NX", "EX", expiry);
		} catch (JedisException e) {
			logger.error("redis error:{}", e);
			handleJedisException(e);
			throw e;
		}
		return "OK".equals(str) ? true : false;
	}

	@Override
	public boolean addHash(String key, Map<String, String> hash, int expiry) {
		JedisCluster jedisCluster = null;
		String str = "false";
		try {
			jedisCluster = jedisClusterClient.getJedisCluster();
			str = jedisCluster.hmset(key, hash);
			jedisCluster.expire(key, expiry);
		} catch (JedisException e) {
			logger.error("redis error:{}", e);
			handleJedisException(e);
			throw e;
		}
		return "OK".equals(str) ? true : false;
	}

	@Override
	public boolean addSet(String key, Map<String, Double> hash) {
		JedisCluster jedisCluster = null;
		long retVal = -1;
		try {
			jedisCluster = jedisClusterClient.getJedisCluster();
			retVal = jedisCluster.zadd(key, hash);
		} catch (JedisException e) {
			logger.error("redis error:{}", e);
			handleJedisException(e);
			throw e;
		}
		return retVal >= 0 ? true : false;
	}

	public boolean addSet(String key, Map<String, Double> hash, int expiry) {
		JedisCluster jedisCluster = null;
		long retVal = -1;
		try {
			jedisCluster = jedisClusterClient.getJedisCluster();
			retVal = jedisCluster.zadd(key, hash);
			jedisCluster.expire(key, expiry);
		} catch (JedisException e) {
			logger.error("redis error:{}", e);
			handleJedisException(e);
			throw e;
		}
		return retVal >= 0 ? true : false;
	}

	@Override
	public double zincrBy(String key, String member) {
		JedisCluster jedisCluster = null;
		double retVal = -1;
		try {
			jedisCluster = jedisClusterClient.getJedisCluster();
			retVal = jedisCluster.zincrby(key, 0, member);
		} catch (JedisException e) {
			logger.error("redis error:{}", e);
			handleJedisException(e);
			throw e;
		}
		return retVal;
	}

	@Override
	public Set<String> zrange(String key, long start, long end) {
		JedisCluster jedisCluster = null;
		Set<String> retVal = null;
		try {
			jedisCluster = jedisClusterClient.getJedisCluster();
			retVal = jedisCluster.zrange(key, start, end);
		} catch (JedisException e) {
			logger.error("redis error:{}", e);
			handleJedisException(e);
			throw e;
		}
		return retVal;
	}

	@Override
	public Set<Tuple> zrangeWithScores(String key, long start, long end) {
		JedisCluster jedisCluster = null;
		Set<Tuple> retVal = null;
		try {
			jedisCluster = jedisClusterClient.getJedisCluster();
			retVal = jedisCluster.zrangeWithScores(key, start, end);
		} catch (JedisException e) {
			logger.error("redis error:{}", e);
			handleJedisException(e);
			throw e;
		}
		return retVal;
	}

	@Override
	public long zrem(String key, String... member) {
		JedisCluster jedisCluster = null;
		long retVal = 0;
		try {
			jedisCluster = jedisClusterClient.getJedisCluster();
			retVal = jedisCluster.zrem(key, member);
		} catch (JedisException e) {
			logger.error("redis error:{}", e);
			handleJedisException(e);
			throw e;
		}
		return retVal;
	}

	@Override
	public double pushOrRem(String key, String value, boolean ishead) {
		JedisCluster jedisCluster = null;
		double retVal = -1;
		try {
			jedisCluster = jedisClusterClient.getJedisCluster();
			if (jedisCluster.lrem(key, 0, value) >= 0) {
				if (ishead) {
					retVal = jedisCluster.lpush(key, value);
				} else {
					retVal = jedisCluster.rpush(key, value);
				}
			}
		} catch (JedisException e) {
			logger.error("redis error:{}", e);
			handleJedisException(e);
			throw e;
		}
		return retVal;
	}

	@Override
	public double push(String key, String value, boolean ishead) {
		JedisCluster jedisCluster = null;
		double retVal = -1;
		try {
			jedisCluster = jedisClusterClient.getJedisCluster();
			if (ishead) {
				retVal = jedisCluster.lpush(key, value);
			} else {
				retVal = jedisCluster.rpush(key, value);
			}
		} catch (JedisException e) {
			logger.error("redis error:{}", e);
			handleJedisException(e);
			throw e;
		}
		return retVal;
	}

	@Override
	public double push(String key, boolean ishead, List<String> values) {
		JedisCluster jedisCluster = null;
		double retVal = -1;
		try {
			jedisCluster = jedisClusterClient.getJedisCluster();
			if (ishead) {
				for (String v : values) {
					retVal = jedisCluster.lpush(key, v);
				}
			} else {
				for (String v : values) {
					retVal = jedisCluster.rpush(key, v);
				}
			}
		} catch (JedisException e) {
			logger.error("redis error:{}", e);
			handleJedisException(e);
			throw e;
		}
		return retVal;
	}

	@Override
	public String popOrRem(String key, boolean ishead) {
		JedisCluster jedisCluster = null;
		String retVal = null;
		try {
			jedisCluster = jedisClusterClient.getJedisCluster();
			if (ishead) {
				retVal = jedisCluster.lpop(key);
			} else {
				retVal = jedisCluster.rpop(key);
			}
			if (!StringUtils.isEmpty(retVal)) {
				jedisCluster.lrem(key, 0, retVal);
			}
		} catch (JedisException e) {
			logger.error("redis error:{}", e);
			handleJedisException(e);
			throw e;
		}
		return retVal;
	}

	@Override
	public String pop(String key, boolean ishead) {
		JedisCluster jedisCluster = null;
		String retVal = null;
		try {
			jedisCluster = jedisClusterClient.getJedisCluster();
			if (ishead) {
				retVal = jedisCluster.lpop(key);
			} else {
				retVal = jedisCluster.rpop(key);
			}
		} catch (JedisException e) {
			logger.error("redis error:{}", e);
			handleJedisException(e);
			throw e;
		}
		return retVal;
	}

	@Override
	public long listLen(String key) {
		JedisCluster jedisCluster = null;
		long retVal = -1;
		try {
			jedisCluster = jedisClusterClient.getJedisCluster();
			retVal = jedisCluster.llen(key);
		} catch (JedisException e) {
			logger.error("redis error:{}", e);
			handleJedisException(e);
			throw e;
		}
		return retVal;
	}

	@Override
	public List<String> lrange(String key, long start, long end) {
		JedisCluster jedisCluster = null;
		List<String> retVal = null;
		try {
			jedisCluster = jedisClusterClient.getJedisCluster();
			retVal = jedisCluster.lrange(key, start, end);
		} catch (JedisException e) {
			logger.error("redis error:{}", e);
			handleJedisException(e);
			throw e;
		}
		return retVal;
	}

	@Override
	public boolean addHash(String key, String field, String value, int expiry) {
		JedisCluster jedisCluster = null;
		Long retval = -1l;
		try {
			jedisCluster = jedisClusterClient.getJedisCluster();
			retval = jedisCluster.hset(key, field, value);
			jedisCluster.expire(key, expiry);
		} catch (JedisException e) {
			logger.error("redis error:{}", e);
			handleJedisException(e);
			throw e;
		}
		return (retval == 0 || retval == 1) ? true : false;
	}

	@Override
	public boolean addHash(String key, String field, String value) {
		JedisCluster jedisCluster = null;
		Long retval = -1l;
		try {
			jedisCluster = jedisClusterClient.getJedisCluster();
			retval = jedisCluster.hset(key, field, value);
		} catch (JedisException e) {
			logger.error("redis error:{}", e);
			handleJedisException(e);
			throw e;
		}
		return (retval == 0 || retval == 1) ? true : false;
	}

	/* (非 Javadoc)
	* <p>Title: addHash</p>
	* <p>Description: </p>
	* @param key
	* @param hash
	* @return
	* @see com.arcsoft.redis.JRedisService#addHash(java.lang.String, java.util.Map)
	*/
	@Override
	public String addHashNX(String key, String startField, String startValue, String endField, String endValue, int seconds) {
		JedisCluster jedisCluster = null;
		String retVal = null;
		try {
			jedisCluster = jedisClusterClient.getJedisCluster();
			if (jedisCluster.hsetnx(key, startField, startValue) > 0) {
				retVal = startValue;
			} else {
				retVal = jedisCluster.hget(key, startField);
			}
			jedisCluster.hsetnx(key, endField, retVal);
			if (seconds > 0)
				jedisCluster.expire(key, seconds);
		} catch (JedisException e) {
			logger.error("redis error:{}", e);
			handleJedisException(e);
			throw e;
		}
		return retVal;
	}

	/* (非 Javadoc)
	* <p>Title: addHash</p>
	* <p>Description: </p>
	* @param key
	* @param hash
	* @return
	* @see com.arcsoft.redis.JRedisService#addHash(java.lang.String, java.util.Map)
	*/
	@Override
	public boolean addHashNX(String key, String field, String value) {
		JedisCluster jedisCluster = null;
		boolean retVal = false;
		try {
			jedisCluster = jedisClusterClient.getJedisCluster();
			retVal = (jedisCluster.hsetnx(key, field, value) > 0);
		} catch (JedisException e) {
			logger.error("redis error:{}", e);
			handleJedisException(e);
			throw e;
		}
		return retVal;
	}

	/* (非 Javadoc)
	* <p>Title: addHash</p>
	* <p>Description: </p>
	* @param key
	* @param hash
	* @return
	* @see com.arcsoft.redis.JRedisService#addHash(java.lang.String, java.util.Map)
	*/
	@Override
	public String addHashNXGet(String key, String field, String value) {
		JedisCluster jedisCluster = null;
		String retVal = null;
		try {
			jedisCluster = jedisClusterClient.getJedisCluster();
			if (jedisCluster.hsetnx(key, field, value) <= 0) {
				retVal = jedisCluster.hget(key, field);
			}
		} catch (JedisException e) {
			logger.error("redis error:{}", e);
			handleJedisException(e);
			throw e;
		}
		return retVal;
	}

	/* (非 Javadoc)
	* <p>Title: addHash</p>
	* <p>Description: </p>
	* @param key
	* @param hash
	* @return
	* @see com.arcsoft.redis.JRedisService#addHash(java.lang.String, java.util.Map)
	*/
	@Override
	public boolean addHashNXDel(String key, String delField, String field, String value) {
		JedisCluster jedisCluster = null;
		boolean retVal = false;
		try {
			jedisCluster = jedisClusterClient.getJedisCluster();
			if (jedisCluster.hsetnx(key, field, value) >= 0) {
				retVal = jedisCluster.hdel(key, delField) > 0;
			}
		} catch (JedisException e) {
			logger.error("redis error:{}", e);
			handleJedisException(e);
			throw e;
		}
		return retVal;
	}

	@Override
	public boolean existsHash(String key, String field) {
		JedisCluster jedisCluster = null;
		boolean retval = false;
		try {
			jedisCluster = jedisClusterClient.getJedisCluster();
			retval = jedisCluster.hexists(key, field);
		} catch (JedisException e) {
			logger.error("redis error:{}", e);
			handleJedisException(e);
			throw e;
		}
		return retval;
	}

	@Override
	public String get(String key) {
		JedisCluster jedisCluster = null;
		String obj = null;
		try {
			jedisCluster = jedisClusterClient.getJedisCluster();
			obj = jedisCluster.get(key);
		} catch (JedisException e) {
			logger.error("redis error:{}", e);
			handleJedisException(e);
			throw e;
		}
		return obj;
	}

	@Override
	public Object getObject(String key) {
		Object obj = null;
		try {
			JedisCluster jedisCluster = jedisClusterClient.getJedisCluster();
			byte[] o = jedisCluster.get(key.getBytes());
			if (o != null) {
				obj = SerializeUtil.unserialize(o);
			}
		} catch (JedisException e) {
			logger.error("redis error:{}", e);
			handleJedisException(e);
			throw e;
		}
		return obj;
	}

	@Override
	public List<String> getHashList(String key, String... fields) {
		JedisCluster jedisCluster = null;
		List<String> list = null;
		try {
			jedisCluster = jedisClusterClient.getJedisCluster();
			list = jedisCluster.hmget(key, fields);
		} catch (JedisException e) {
			logger.error("redis error:{}", e);
			handleJedisException(e);
			throw e;
		}
		return list;
	}

	@Override
	public Map<String, String> getHashNXGet(String key, String field, String value) {
		JedisCluster jedisCluster = null;
		Map<String, String> map = null;
		try {
			jedisCluster = jedisClusterClient.getJedisCluster();
			if (jedisCluster.hsetnx(key, field, value) > 0) {
				map = jedisCluster.hgetAll(key);
			} else {
				map = new HashMap<String, String>();
				map.put(field, "-1");
			}
		} catch (JedisException e) {
			logger.error("redis error:{}", e);
			handleJedisException(e);
			throw e;
		}
		return map;
	}

	@Override
	public Map<String, String> getHashAll(String key) {
		JedisCluster jedisCluster = null;
		Map<String, String> map = null;
		try {
			jedisCluster = jedisClusterClient.getJedisCluster();
			map = jedisCluster.hgetAll(key);
		} catch (JedisException e) {
			logger.error("redis error:{}", e);
			handleJedisException(e);
			throw e;
		}
		return map;
	}

	@Override
	public long getHashLen(String key) {
		JedisCluster jedisCluster = null;
		long retVal = 0;
		try {
			jedisCluster = jedisClusterClient.getJedisCluster();
			retVal = jedisCluster.hlen(key);
		} catch (JedisException e) {
			logger.error("redis error:{}", e);
			handleJedisException(e);
			throw e;
		}
		return retVal;
	}

	@Override
	public Set<String> getSetAll(String key) {
		JedisCluster jedisCluster = null;
		Set<String> set = null;
		try {
			jedisCluster = jedisClusterClient.getJedisCluster();
			set = jedisCluster.zrange(key, 0, -1);
		} catch (JedisException e) {
			logger.error("redis error:{}", e);
			handleJedisException(e);
			throw e;
		}
		return set;
	}

	@Override
	public Set<Tuple> getSetAllWithScores(String key) {
		JedisCluster jedisCluster = null;
		Set<Tuple> set = null;
		try {
			jedisCluster = jedisClusterClient.getJedisCluster();
			set = jedisCluster.zrangeWithScores(key, 0, -1);
		} catch (JedisException e) {
			logger.error("redis error:{}", e);
			handleJedisException(e);
			throw e;
		}
		return set;
	}

	@Override
	public String getHash(String key, String field) {
		JedisCluster jedisCluster = null;
		String str = null;
		try {
			jedisCluster = jedisClusterClient.getJedisCluster();
			str = jedisCluster.hget(key, field);
		} catch (JedisException e) {
			logger.error("redis error:{}", e);
			handleJedisException(e);
			throw e;
		}
		return str;
	}

	@Override
	public <T> T get(String key, Class<T> clazz) {
		JedisCluster jedisCluster = null;
		try {
			jedisCluster = jedisClusterClient.getJedisCluster();
			String value = jedisCluster.get(key);
			if (value != null && value.length() > 0) {
				JSONObject jsonObject = new JSONObject();
				return jsonObject.getObject(value, clazz);
			}
		} catch (JedisException e) {
			logger.error("redis error:{}", e);
			handleJedisException(e);
			throw e;
		}
		return null;
	}

	protected void handleJedisException(JedisException jedisException) {
		if (jedisException instanceof JedisConnectionException) {
			logger.error("Redis connection  lost.", jedisException);
		} else if (jedisException instanceof JedisDataException) {
			if ((jedisException.getMessage() != null) && (jedisException.getMessage().indexOf("READONLY") != -1)) {
				logger.error("Redis connection  are read-only slave.", jedisException);
			}
		} else {
			logger.error("Jedis exception happen.", jedisException);
		}
	}

	@Override
	public Long hIncrBy(String key, String field, long value) {
		JedisCluster jedisCluster = null;
		Long result = 0L;
		try {
			jedisCluster = jedisClusterClient.getJedisCluster();
			result = jedisCluster.hincrBy(key, field, value);
		} catch (JedisException e) {
			logger.error("redis error:{}", e);
			handleJedisException(e);
			throw e;
		}
		return result;
	}

	@Override
	public Long hIncrBy(String key, Map<String, Long> map, int seconds) {
		JedisCluster jedisCluster = null;
		Long result = 0L;
		try {
			jedisCluster = jedisClusterClient.getJedisCluster();
			Iterator<Map.Entry<String, Long>> it = map.entrySet().iterator();
			while (it.hasNext()) {
				Map.Entry<String, Long> entry = it.next();
				result += jedisCluster.hincrBy(key, entry.getKey(), entry.getValue());
			}
			if (seconds != 0) {
				jedisCluster.expire(key, seconds);
			}
		} catch (JedisException e) {
			logger.error("redis error:{}", e);
			handleJedisException(e);
			throw e;
		}
		return result;
	}

	@Override
	public Double zIncrBy(String key, String field, long value) {
		JedisCluster jedisCluster = null;
		Double result = 0.0;
		try {
			jedisCluster = jedisClusterClient.getJedisCluster();
			result = jedisCluster.zincrby(key, value, field);
		} catch (JedisException e) {
			logger.error("redis error:{}", e);
			handleJedisException(e);
			throw e;
		}
		return result;
	}
}