package com.ideal.manage.cache.bean;

/**
 * Desc:    实时获取活动数据
 * Author: Iron
 * CreateDate: 2016-11-14
 * CopyRight: Beijing Yunzong Co., Ltd.
 */
public class ReportData {


    private long equipId;
    private long thresholdId;

    private int value;

    public long getEquipId() {
        return equipId;
    }

    public void setEquipId(long equipId) {
        this.equipId = equipId;
    }

    public long getThresholdId() {
        return thresholdId;
    }

    public void setThresholdId(long thresholdId) {
        this.thresholdId = thresholdId;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "ReportData{" +
                "equipId=" + equipId +
                ", thresholdId=" + thresholdId +
                ", value=" + value +
                '}';
    }

}
