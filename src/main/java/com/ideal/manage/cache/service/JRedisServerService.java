/**   
* @Title: JRedisServerService.java 
* @Package com.beaconem.iot.redis.service
* @Description: TODO(用一句话描述该文件做什么) 
* @author hb8588  
* @date 2016年9月26日 下午12:46:16 
* @version V1.0   
*/
package com.ideal.manage.cache.service;

/** 
* @ClassName: JRedisServerService 
* @Description: TODO(这里用一句话描述这个类的作用) 
* @author hb8588 
* @date 2016年9月26日 下午12:46:16 
*  
*/
public interface JRedisServerService {
	/**
	 * 
	* @Title: getServerIP; 
	* @Description: 获取服务IP地址; 
	* @param @param serverId
	* @param @return;
	* @return String;
	* @throws;
	 */
	public String getServerIP(String serverId);
}
