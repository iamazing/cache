/**   
* @Title: JRedisService.java 
* @Package com.beaconem.iot.broker.service.redis.impl
* @Description: TODO(用一句话描述该文件做什么) 
* @author hb8588  
* @date 2016年9月22日 下午4:09:53 
* @version V1.0   
*/
package com.ideal.manage.cache.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.ideal.manage.cache.config.CommonConstants;
import com.ideal.manage.cache.service.JRedisService;
import com.ideal.manage.cache.tool.SerializeUtil;
import org.apache.commons.lang3.StringUtils;
import redis.clients.jedis.ShardedJedis;
import redis.clients.jedis.Tuple;
import redis.clients.jedis.exceptions.JedisConnectionException;
import redis.clients.jedis.exceptions.JedisDataException;
import redis.clients.jedis.exceptions.JedisException;

import java.util.*;

/**
 * @ClassName: JRedisService
 * @Description: TODO(这里用一句话描述这个类的作用)
 * @author hb8588
 * @date 2016年9月22日 下午4:09:53
 * 
 */
public class JRedisServiceImpl extends BaseService implements JRedisService {
	/**
	 * @Title: @Description: TODO(这里用一句话描述这个方法的作用) @author:hb @return @throws
	 */
	public JRedisServiceImpl() {
		super();
	}

	/**
	 * @Title: @Description: TODO(这里用一句话描述这个方法的作用) @author:hb @param localAddress @param port @param jedisClusterClient @return @throws
	 */
	public JRedisServiceImpl(String port, JedisClusterClient jedisClusterClient) {
		super();
		this.port = port;
		this.jedisClusterClient = jedisClusterClient;
	}

	/**
	 * 
	 * @Title: init;
	 * @Description: 初始化服务信息
	 * @param ;
	 * @return void; @throws;
	 */
	public void init() {
		ShardedJedis jedis = null;
		try {
			jedis = jedisClusterClient.getJedisResource();
			if (jedis.hexists(CommonConstants.SERVER_REDIS_HSET_NAME, this.getLocalIpPort())) {
				setServerId(jedis.hget(CommonConstants.SERVER_REDIS_HSET_NAME, this.getLocalIpPort()));
			} else {
				long number = jedis.incr(CommonConstants.SERVER_REDIS_HSET_NAME_INCR);
				jedis.hset(CommonConstants.SERVER_REDIS_HSET_NAME, this.getLocalIpPort(), String.valueOf(number));
				setServerId(String.valueOf(number));
			}
		} catch (JedisException e) {
			// 销毁对象
			logger.error("redis error:{}", e);
			handleJedisException(e);
			throw e;
		} finally {
			closeResource(jedis);
		}
	}

	/**
	 * 
	 * @Title: getServerList;
	 * @Description: 获取服务IP地址列表;
	 * @param @param deviceId
	 * @param @return;
	 * @return Map<String,String>; @throws;
	 */
	public Map<String, String> getServerList() {

		ShardedJedis jedis = null;
		try {
			jedis = jedisClusterClient.getJedisResource();
			return jedis.hgetAll(CommonConstants.SERVER_REDIS_HSET_NAME);
		} catch (JedisException e) {
			// 销毁对象
			logger.error("redis error:{}", e);
			handleJedisException(e);
			throw e;
		} finally {
			closeResource(jedis);
		}
	}

	protected void handleJedisException(JedisException jedisException) {
		if (jedisException instanceof JedisConnectionException) {
			logger.error("Redis connection  lost.", jedisException);
		} else if (jedisException instanceof JedisDataException) {
			if ((jedisException.getMessage() != null) && (jedisException.getMessage().indexOf("READONLY") != -1)) {
				logger.error("Redis connection  are read-only slave.", jedisException);
			}
		} else {
			logger.error("Jedis exception happen.", jedisException);
		}

	}

	private void closeResource(ShardedJedis jedis) {
		try {
			if (null != jedis) {
				jedis.close();
			}
		} catch (Exception e) {
			logger.error("return back jedis failed, will fore close the jedis.", e);
			jedisClusterClient.initNotCluster();
		}
	}

	@Override
	public boolean add(String key, Object value) {
		ShardedJedis jedis = null;
		String str = "false";
		try {
			jedis = jedisClusterClient.getJedisResource();
			str = jedis.set(key.getBytes(), SerializeUtil.serialize(value));
		} catch (JedisException e) {
			logger.error("redis error:{}", e);
			handleJedisException(e);
			throw e;
		} finally {
			closeResource(jedis);
		}
		return "OK".equals(str) ? true : false;
	}

	@Override
	public boolean addString(String key, String value) {
		ShardedJedis jedis = null;
		String str = "false";
		try {
			jedis = jedisClusterClient.getJedisResource();
			str = jedis.set(key, value);
		} catch (JedisException e) {
			logger.error("redis error:{}", e);
			handleJedisException(e);
			throw e;
		} finally {
			closeResource(jedis);
		}
		return "OK".equals(str) ? true : false;
	}

	@Override
	public boolean addString(String key, String value, long expiry) {
		ShardedJedis jedis = null;
		String str = "false";
		try {
			jedis = jedisClusterClient.getJedisResource();
			str = jedis.setex(key, Integer.parseInt(expiry + ""), value);
		} catch (JedisException e) {
			logger.error("redis error:{}", e);
			handleJedisException(e);
			throw e;
		} finally {
			closeResource(jedis);
		}
		return "OK".equals(str) ? true : false;
	}

	@Override
	public Long del(String key) {
		ShardedJedis jedis = null;
		try {
			jedis = jedisClusterClient.getJedisResource();
			return jedis.del(key.getBytes());
		} catch (JedisException e) {
			logger.error("redis error:{}", e);
			handleJedisException(e);
			throw e;
		} finally {
			closeResource(jedis);
		}
	}

	@Override
	public Long incr(String key) {
		ShardedJedis jedis = null;
		try {
			jedis = jedisClusterClient.getJedisResource();
			return jedis.incr(key.getBytes());
		} catch (JedisException e) {
			logger.error("redis error:{}", e);
			handleJedisException(e);
			throw e;
		} finally {
			closeResource(jedis);
		}
	}

	@Override
	public Long incrBy(String key, long value) {
		ShardedJedis jedis = null;
		try {
			jedis = jedisClusterClient.getJedisResource();
			return jedis.incrBy(key, value);
		} catch (JedisException e) {
			logger.error("redis error:{}", e);
			handleJedisException(e);
			throw e;
		} finally {
			closeResource(jedis);
		}
	}

	@Override
	public Long expire(String key, int seconds) {
		ShardedJedis jedis = null;
		try {
			jedis = jedisClusterClient.getJedisResource();
			return jedis.expire(key, seconds);
		} catch (JedisException e) {
			logger.error("redis error:{}", e);
			handleJedisException(e);
			throw e;
		} finally {
			closeResource(jedis);
		}
	}

	@Override
	public Long ttl(String key) {
		ShardedJedis jedis = null;
		try {
			jedis = jedisClusterClient.getJedisResource();
			return jedis.ttl(key);
		} catch (JedisException e) {
			logger.error("redis error:{}", e);
			handleJedisException(e);
			throw e;
		} finally {
			closeResource(jedis);
		}
	}

	@Override
	public Long expireAndTTL(String key, int seconds) {
		ShardedJedis jedis = null;
		try {
			jedis = jedisClusterClient.getJedisResource();
			long ttl = jedis.ttl(key);
			if (ttl > 0)
				return ttl;
			return jedis.expire(key, seconds);
		} catch (JedisException e) {
			logger.error("redis error:{}", e);
			handleJedisException(e);
			throw e;
		} finally {
			closeResource(jedis);
		}
	}

	@Override
	public boolean add(String key, Object value, long expiry) {
		ShardedJedis jedis = null;
		String str = "false";
		try {
			jedis = jedisClusterClient.getJedisResource();
			jedis.del(key);
			str = jedis.set(key.getBytes(), SerializeUtil.serialize(value), "NX".getBytes(), "EX".getBytes(), expiry);
		} catch (JedisException e) {
			logger.error("redis error:{}", e);
			handleJedisException(e);
			throw e;
		} finally {
			closeResource(jedis);
		}
		return "OK".equals(str) ? true : false;
	}

	@Override
	public boolean add(String key, String value, long expiry) {
		ShardedJedis jedis = null;
		String str = "false";
		try {
			jedis = jedisClusterClient.getJedisResource();
			jedis.del(key);
			str = jedis.set(key, value, "NX", "EX", expiry);
		} catch (JedisException e) {
			logger.error("redis error:{}", e);
			handleJedisException(e);
			throw e;
		} finally {
			closeResource(jedis);
		}
		return "OK".equals(str) ? true : false;
	}

	@Override
	public boolean addnx(String key, String value, int expiry) {
		ShardedJedis jedis = null;
		long retVal = -1;
		try {
			jedis = jedisClusterClient.getJedisResource();
			retVal = jedis.setnx(key, value);
			jedis.expire(key, expiry);
		} catch (JedisException e) {
			logger.error("redis error:{}", e);
			handleJedisException(e);
			throw e;
		} finally {
			closeResource(jedis);
		}
		return retVal > 0;
	}

	@Override
	public double zincrBy(String key, String member) {
		ShardedJedis jedis = null;
		double retVal = -1;
		try {
			jedis = jedisClusterClient.getJedisResource();
			retVal = jedis.zincrby(key, 0, member);
		} catch (JedisException e) {
			logger.error("redis error:{}", e);
			handleJedisException(e);
			throw e;
		} finally {
			closeResource(jedis);
		}
		return retVal;
	}

	@Override
	public Set<String> zrange(String key, long start, long end) {
		ShardedJedis jedis = null;
		Set<String> retVal = null;
		try {
			jedis = jedisClusterClient.getJedisResource();
			retVal = jedis.zrange(key, start, end);
		} catch (JedisException e) {
			logger.error("redis error:{}", e);
			handleJedisException(e);
			throw e;
		} finally {
			closeResource(jedis);
		}
		return retVal;
	}

	@Override
	public Set<Tuple> zrangeWithScores(String key, long start, long end) {
		ShardedJedis jedis = null;
		Set<Tuple> retVal = null;
		try {
			jedis = jedisClusterClient.getJedisResource();
			retVal = jedis.zrangeWithScores(key, start, end);
		} catch (JedisException e) {
			logger.error("redis error:{}", e);
			handleJedisException(e);
			throw e;
		} finally {
			closeResource(jedis);
		}
		return retVal;
	}

	@Override
	public long zrem(String key, String... members) {
		ShardedJedis jedis = null;
		long retVal = 0;
		try {
			jedis = jedisClusterClient.getJedisResource();
			retVal = jedis.zrem(key, members);
		} catch (JedisException e) {
			logger.error("redis error:{}", e);
			handleJedisException(e);
			throw e;
		} finally {
			closeResource(jedis);
		}
		return retVal;
	}

	@Override
	public String get(String key) {
		ShardedJedis jedis = null;
		String obj = null;
		try {
			jedis = jedisClusterClient.getJedisResource();
			obj = jedis.get(key);
		} catch (JedisException e) {
			logger.error("redis error:{}", e);
			handleJedisException(e);
			throw e;
		} finally {
			closeResource(jedis);
		}
		return obj;
	}

	@Override
	public Object getObject(String key) {
		ShardedJedis jedis = null;
		Object obj = null;
		try {
			jedis = jedisClusterClient.getJedisResource();
			byte[] o = jedis.get(key.getBytes());
			if (o != null) {
				obj = SerializeUtil.unserialize(o);
			}
		} catch (JedisException e) {
			logger.error("redis error:{}", e);
			handleJedisException(e);
			throw e;
		} finally {
			closeResource(jedis);
		}
		return obj;
	}

	/* (非 Javadoc)
	* <p>Title: addHash</p>  
	* <p>Description: </p>  
	* @param key
	* @param field
	* @param value
	* @param expiry
	* @return  
	* @see com.arcsoft.redis.JRedisService#addHash(java.lang.String, java.lang.String, java.lang.String, int)  
	*/
	@Override
	public boolean addHash(String key, String field, String value, int expiry) {
		ShardedJedis jedis = null;
		long retVal = -1l;
		try {
			jedis = jedisClusterClient.getJedisResource();
			retVal = jedis.hset(key, field, value);
			jedis.expire(key, expiry);
		} catch (JedisException e) {
			logger.error("redis error:{}", e);
			handleJedisException(e);
			throw e;
		} finally {
			closeResource(jedis);
		}
		return (retVal == 0 || retVal == 1) ? true : false;
	}

	/* (非 Javadoc)  
	* <p>Title: addHash</p>  
	* <p>Description: </p>  
	* @param key
	* @param field
	* @param value
	* @param expiry
	* @return  
	* @see com.arcsoft.redis.JRedisService#addHash(java.lang.String, java.lang.String, java.lang.String, int)  
	*/
	@Override
	public boolean addHash(String key, String field, String value) {
		ShardedJedis jedis = null;
		long retVal = -1l;
		try {
			jedis = jedisClusterClient.getJedisResource();
			retVal = jedis.hset(key, field, value);
		} catch (JedisException e) {
			logger.error("redis error:{}", e);
			handleJedisException(e);
			throw e;
		} finally {
			closeResource(jedis);
		}
		return (retVal == 0 || retVal == 1) ? true : false;
	}

	/* (非 Javadoc)  
	* <p>Title: addHash</p>  
	* <p>Description: </p>  
	* @param key
	* @param hash
	* @param expiry
	* @return  
	* @see com.arcsoft.redis.JRedisService#addHash(java.lang.String, java.util.Map, int)  
	*/
	@Override
	public boolean addHash(String key, Map<String, String> hash, int expiry) {
		ShardedJedis jedis = null;
		String str = "false";
		try {
			jedis = jedisClusterClient.getJedisResource();
			str = jedis.hmset(key, hash);
			jedis.expire(key, expiry);
		} catch (JedisException e) {
			logger.error("redis error:{}", e);
			handleJedisException(e);
			throw e;
		} finally {
			closeResource(jedis);
		}
		return "OK".equals(str) ? true : false;
	}

	/* (非 Javadoc)  
	* <p>Title: addHash</p>  
	* <p>Description: </p>  
	* @param key
	* @param hash
	* @return  
	* @see com.arcsoft.redis.JRedisService#addHash(java.lang.String, java.util.Map)  
	*/
	@Override
	public boolean addHash(String key, Map<String, String> hash) {
		ShardedJedis jedis = null;
		String str = "false";
		try {
			jedis = jedisClusterClient.getJedisResource();
			str = jedis.hmset(key, hash);
		} catch (JedisException e) {
			logger.error("redis error:{}", e);
			handleJedisException(e);
			throw e;
		} finally {
			closeResource(jedis);
		}
		return "OK".equals(str) ? true : false;
	}

	/* (非 Javadoc)  
	* <p>Title: addHash</p>  
	* <p>Description: </p>  
	* @param key
	* @param hash
	* @return  
	* @see com.arcsoft.redis.JRedisService#addHash(java.lang.String, java.util.Map)  
	*/
	@Override
	public String addHashNX(String key, String startField, String startValue, String endField, String endValue, int seconds) {
		ShardedJedis jedis = null;
		String retVal = null;
		try {
			jedis = jedisClusterClient.getJedisResource();
			if (jedis.hsetnx(key, startField, startValue) > 0) {
				retVal = startValue;
			} else {
				retVal = jedis.hget(key, startField);
			}
			jedis.hsetnx(key, endField, retVal);
			if (seconds > 0)
				jedis.expire(key, seconds);
		} catch (JedisException e) {
			logger.error("redis error:{}", e);
			handleJedisException(e);
			throw e;
		} finally {
			closeResource(jedis);
		}
		return retVal;
	}

	/* (非 Javadoc)  
	* <p>Title: addHash</p>  
	* <p>Description: </p>  
	* @param key
	* @param hash
	* @return  
	* @see com.arcsoft.redis.JRedisService#addHash(java.lang.String, java.util.Map)  
	*/
	@Override
	public boolean addHashNX(String key, String field, String value) {
		ShardedJedis jedis = null;
		boolean retVal = false;
		try {
			jedis = jedisClusterClient.getJedisResource();
			retVal = (jedis.hsetnx(key, field, value) > 0);
		} catch (JedisException e) {
			logger.error("redis error:{}", e);
			handleJedisException(e);
			throw e;
		} finally {
			closeResource(jedis);
		}
		return retVal;
	}

	/* (非 Javadoc)  
	* <p>Title: addHash</p>  
	* <p>Description: </p>  
	* @param key
	* @param hash
	* @return  
	* @see com.arcsoft.redis.JRedisService#addHash(java.lang.String, java.util.Map)  
	*/
	@Override
	public String addHashNXGet(String key, String field, String value) {
		ShardedJedis jedis = null;
		String retVal = null;
		try {
			jedis = jedisClusterClient.getJedisResource();
			if (jedis.hsetnx(key, field, value) <= 0) {
				retVal = jedis.hget(key, field);
			}
		} catch (JedisException e) {
			logger.error("redis error:{}", e);
			handleJedisException(e);
			throw e;
		} finally {
			closeResource(jedis);
		}
		return retVal;
	}

	/* (非 Javadoc)  
	* <p>Title: addHash</p>  
	* <p>Description: </p>  
	* @param key
	* @param hash
	* @return  
	* @see com.arcsoft.redis.JRedisService#addHash(java.lang.String, java.util.Map)  
	*/
	@Override
	public boolean addHashNXDel(String key, String delField, String field, String value) {
		ShardedJedis jedis = null;
		boolean retVal = false;
		try {
			jedis = jedisClusterClient.getJedisResource();
			if (jedis.hsetnx(key, field, value) >= 0) {
				retVal = jedis.hdel(key, delField) > 0;
			}
		} catch (JedisException e) {
			logger.error("redis error:{}", e);
			handleJedisException(e);
			throw e;
		} finally {
			closeResource(jedis);
		}
		return retVal;
	}

	@Override
	public double pushOrRem(String key, String value, boolean isHead) {
		ShardedJedis jedis = null;
		double retVal = -1;
		try {
			jedis = jedisClusterClient.getJedisResource();
			if (jedis.lrem(key, 0, value) >= 0) {
				if (isHead) {
					retVal = jedis.lpush(key, value);
				} else {
					retVal = jedis.rpush(key, value);
				}
			}
		} catch (JedisException e) {
			logger.error("redis error:{}", e);
			handleJedisException(e);
			throw e;
		} finally {
			closeResource(jedis);
		}
		return retVal;
	}

	@Override
	public double push(String key, String value, boolean isHead) {
		ShardedJedis jedis = null;
		double retVal = -1;
		try {
			jedis = jedisClusterClient.getJedisResource();
			if (isHead) {
				retVal = jedis.lpush(key, value);
			} else {
				retVal = jedis.rpush(key, value);
			}
		} catch (JedisException e) {
			logger.error("redis error:{}", e);
			handleJedisException(e);
			throw e;
		} finally {
			closeResource(jedis);
		}
		return retVal;
	}

	@Override
	public double push(String key, boolean isHead, List<String> values) {
		ShardedJedis jedis = null;
		double retVal = -1;
		try {
			jedis = jedisClusterClient.getJedisResource();
			if (isHead) {
				for (String v : values) {
					retVal = jedis.lpush(key, v);
				}
			} else {
				for (String v : values) {
					retVal = jedis.rpush(key, v);
				}
			}
		} catch (JedisException e) {
			logger.error("redis error:{}", e);
			handleJedisException(e);
			throw e;
		} finally {
			closeResource(jedis);
		}
		return retVal;
	}

	@Override
	public String popOrRem(String key, boolean isHead) {
		ShardedJedis jedis = null;
		String retVal = null;
		try {
			jedis = jedisClusterClient.getJedisResource();
			if (isHead) {
				retVal = jedis.lpop(key);
			} else {
				retVal = jedis.rpop(key);
			}
			if (!StringUtils.isEmpty(retVal)) {
				jedis.lrem(key, 0, retVal);
			}
		} catch (JedisException e) {
			logger.error("redis error:{}", e);
			handleJedisException(e);
			throw e;
		} finally {
			closeResource(jedis);
		}
		return retVal;
	}

	@Override
	public String pop(String key, boolean isHead) {
		ShardedJedis jedis = null;
		String retVal = null;
		try {
			jedis = jedisClusterClient.getJedisResource();
			if (isHead) {
				retVal = jedis.lpop(key);
			} else {
				retVal = jedis.rpop(key);
			}
		} catch (JedisException e) {
			logger.error("redis error:{}", e);
			handleJedisException(e);
			throw e;
		} finally {
			closeResource(jedis);
		}
		return retVal;
	}

	@Override
	public long listLen(String key) {
		ShardedJedis jedis = null;
		long retVal = -1;
		try {
			jedis = jedisClusterClient.getJedisResource();
			retVal = jedis.llen(key);
		} catch (JedisException e) {
			logger.error("redis error:{}", e);
			handleJedisException(e);
			throw e;
		} finally {
			closeResource(jedis);
		}
		return retVal;
	}

	@Override
	public List<String> lrange(String key, long start, long end) {
		ShardedJedis jedis = null;
		List<String> retVal = null;
		try {
			jedis = jedisClusterClient.getJedisResource();
			retVal = jedis.lrange(key, start, end);
		} catch (JedisException e) {
			logger.error("redis error:{}", e);
			handleJedisException(e);
			throw e;
		} finally {
			closeResource(jedis);
		}
		return retVal;
	}

	@Override
	public Long delHash(String key, String... field) {
		ShardedJedis jedis = null;
		try {
			jedis = jedisClusterClient.getJedisResource();
			return jedis.hdel(key, field);
		} catch (JedisException e) {
			logger.error("redis error:{}", e);
			handleJedisException(e);
			throw e;
		} finally {
			closeResource(jedis);
		}
	}

	@Override
	public Long delHashAndExp(String key, int expiry, String... field) {
		ShardedJedis jedis = null;
		try {
			jedis = jedisClusterClient.getJedisResource();
			if (jedis.hdel(key, field) > 0) {
				return jedis.expire(key, expiry);
			}
		} catch (JedisException e) {
			logger.error("redis error:{}", e);
			handleJedisException(e);
			throw e;
		} finally {
			closeResource(jedis);
		}
		return 0l;
	}

	/* (非 Javadoc)  
	* <p>Title: getHash</p>  
	* <p>Description: </p>  
	* @param key
	* @param field
	* @return  
	* @see com.arcsoft.redis.JRedisService#getHash(java.lang.String, java.lang.String)  
	*/
	@Override
	public String getHash(String key, String field) {
		ShardedJedis jedis = null;
		String str = null;
		try {
			jedis = jedisClusterClient.getJedisResource();
			str = jedis.hget(key, field);
		} catch (JedisException e) {
			logger.error("redis error:{}", e);
			handleJedisException(e);
			throw e;
		} finally {
			closeResource(jedis);
		}
		return str;
	}

	/* (非 Javadoc)  
	* <p>Title: getHashList</p>  
	* <p>Description: </p>  
	* @param key
	* @param fields
	* @return  
	* @see com.arcsoft.redis.JRedisService#getHashList(java.lang.String, java.lang.String[])  
	*/
	@Override
	public List<String> getHashList(String key, String... fields) {
		ShardedJedis jedis = null;
		List<String> list = null;
		try {
			jedis = jedisClusterClient.getJedisResource();
			list = jedis.hmget(key, fields);
		} catch (JedisException e) {
			logger.error("redis error:{}", e);
			handleJedisException(e);
			throw e;
		} finally {
			closeResource(jedis);
		}
		return list;
	}

	/* (非 Javadoc)  
	* <p>Title: exists</p>  
	* <p>Description: </p>  
	* @param key
	* @return  
	* @see com.arcsoft.redis.JRedisService#exists(java.lang.String)  
	*/
	@Override
	public boolean exists(String key) {
		ShardedJedis jedis = null;
		boolean retVal = false;
		try {
			jedis = jedisClusterClient.getJedisResource();
			retVal = jedis.exists(key);
		} catch (JedisException e) {
			logger.error("redis error:{}", e);
			handleJedisException(e);
			throw e;
		} finally {
			closeResource(jedis);
		}
		return retVal;
	}

	/* (非 Javadoc)  
	* <p>Title: existsHash</p>  
	* <p>Description: </p>  
	* @param key
	* @param field
	* @return  
	* @see com.arcsoft.redis.JRedisService#existsHash(java.lang.String, java.lang.String)  
	*/
	@Override
	public boolean existsHash(String key, String field) {
		ShardedJedis jedis = null;
		boolean retVal = false;
		try {
			jedis = jedisClusterClient.getJedisResource();
			retVal = jedis.hexists(key, field);
		} catch (JedisException e) {
			logger.error("redis error:{}", e);
			handleJedisException(e);
			throw e;
		} finally {
			closeResource(jedis);
		}
		return retVal;
	}

	/* (非 Javadoc)  
	* <p>Title: getHashAll</p>  
	* <p>Description: </p>  
	* @param key
	* @return  
	* @see com.arcsoft.redis.JRedisService#getHashAll(java.lang.String)  
	*/
	@Override
	public Map<String, String> getHashNXGet(String key, String field, String value) {
		ShardedJedis jedis = null;
		Map<String, String> map = null;
		try {
			jedis = jedisClusterClient.getJedisResource();
			if (jedis.hsetnx(key, field, value) > 0) {
				map = jedis.hgetAll(key);
			} else {
				map = new HashMap<String, String>();
				map.put(field, "-1");
			}
		} catch (JedisException e) {
			logger.error("redis error:{}", e);
			handleJedisException(e);
			throw e;
		} finally {
			closeResource(jedis);
		}
		return map;
	}

	/* (非 Javadoc)  
	* <p>Title: getHashAll</p>  
	* <p>Description: </p>  
	* @param key
	* @return  
	* @see com.arcsoft.redis.JRedisService#getHashAll(java.lang.String)  
	*/
	@Override
	public Map<String, String> getHashAll(String key) {
		ShardedJedis jedis = null;
		Map<String, String> map = null;
		try {
			jedis = jedisClusterClient.getJedisResource();
			map = jedis.hgetAll(key);
		} catch (JedisException e) {
			logger.error("redis error:{}", e);
			handleJedisException(e);
			throw e;
		} finally {
			closeResource(jedis);
		}
		return map;
	}

	/**
	 * 
	 */
	@Override
	public long getHashLen(String key) {
		ShardedJedis jedis = null;
		long retVal = 0;
		try {
			jedis = jedisClusterClient.getJedisResource();
			retVal = jedis.hlen(key);
		} catch (JedisException e) {
			logger.error("redis error:{}", e);
			handleJedisException(e);
			throw e;
		} finally {
			closeResource(jedis);
		}
		return retVal;
	}

	/* (非 Javadoc)  
	* <p>Title: addSet</p>  
	* <p>Description: </p>  
	* @param key
	* @param hash
	* @return  
	* @see com.arcsoft.redis.JRedisService#addSet(java.lang.String, java.util.Map)  
	*/
	@Override
	public boolean addSet(String key, Map<String, Double> hash) {
		ShardedJedis jedis = null;
		long retVal = -1;
		try {
			jedis = jedisClusterClient.getJedisResource();
			retVal = jedis.zadd(key, hash);
		} catch (JedisException e) {
			logger.error("redis error:{}", e);
			handleJedisException(e);
			throw e;
		} finally {
			closeResource(jedis);
		}
		return retVal > 0 ? true : false;
	}

	@Override
	public boolean addSet(String key, Map<String, Double> hash, int expiry) {
		ShardedJedis jedis = null;
		long retVal = -1;
		try {
			jedis = jedisClusterClient.getJedisResource();
			retVal = jedis.zadd(key, hash);
			jedis.expire(key, expiry);
		} catch (JedisException e) {
			logger.error("redis error:{}", e);
			handleJedisException(e);
			throw e;
		} finally {
			closeResource(jedis);
		}
		return retVal > 0 ? true : false;
	}

	/* (非 Javadoc)  
	* <p>Title: getSetAll</p>  
	* <p>Description: </p>  
	* @param key
	* @return  
	* @see com.arcsoft.redis.JRedisService#getSetAll(java.lang.String)  
	*/
	@Override
	public Set<String> getSetAll(String key) {
		ShardedJedis jedis = null;
		Set<String> set = null;
		try {
			jedis = jedisClusterClient.getJedisResource();
			set = jedis.zrange(key, 0, -1);
		} catch (JedisException e) {
			logger.error("redis error:{}", e);
			handleJedisException(e);
			throw e;
		} finally {
			closeResource(jedis);
		}
		return set;
	}

	@Override
	public Set<Tuple> getSetAllWithScores(String key) {
		ShardedJedis jedis = null;
		Set<Tuple> set = null;
		try {
			jedis = jedisClusterClient.getJedisResource();
			set = jedis.zrangeWithScores(key, 0, -1);
		} catch (JedisException e) {
			logger.error("redis error:{}", e);
			handleJedisException(e);
			throw e;
		} finally {
			closeResource(jedis);
		}
		return set;
	}

	@Override
	public <T> T get(String key, Class<T> clazz) {
		ShardedJedis jedis = null;
		try {
			jedis = jedisClusterClient.getJedisResource();
			String value = jedis.get(key);
			if (value != null && value.length() > 0) {
				JSONObject jsonObject = new JSONObject();
				return jsonObject.getObject(value, clazz);
			}
		} catch (JedisException e) {
			logger.error("redis error:{}", e);
			handleJedisException(e);
			throw e;
		} finally {
			closeResource(jedis);
		}
		return null;
	}

	@Override
	public Long hIncrBy(String key, String field, long value) {
		ShardedJedis jedis = null;
		Long result = 0L;
		try {
			jedis = jedisClusterClient.getJedisResource();
			result = jedis.hincrBy(key, field, value);
		} catch (JedisException e) {
			logger.error("redis error:{}", e);
			handleJedisException(e);
			throw e;
		} finally {
			closeResource(jedis);
		}
		return result;
	}

	@Override
	public Long hIncrBy(String key, Map<String, Long> map, int seconds) {
		ShardedJedis jedis = null;
		Long result = 0L;
		try {
			jedis = jedisClusterClient.getJedisResource();
			Iterator<Map.Entry<String, Long>> it = map.entrySet().iterator();
			while (it.hasNext()) {
				Map.Entry<String, Long> entry = it.next();
				result += jedis.hincrBy(key, entry.getKey(), entry.getValue());
			}
			if (seconds != 0) {
				jedis.expire(key, seconds);
			}
		} catch (JedisException e) {
			logger.error("redis error:{}", e);
			handleJedisException(e);
			throw e;
		} finally {
			closeResource(jedis);
		}
		return result;
	}

	@Override
	public Double zIncrBy(String key, String field, long value) {
		ShardedJedis jedis = null;
		Double result = 0.0;
		try {
			jedis = jedisClusterClient.getJedisResource();
			result = jedis.zincrby(key, value, field);
		} catch (JedisException e) {
			logger.error("redis error:{}", e);
			handleJedisException(e);
			throw e;
		} finally {
			closeResource(jedis);
		}
		return result;
	}
}
