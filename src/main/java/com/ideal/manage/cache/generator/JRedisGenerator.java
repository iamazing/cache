/**   
* @Title: JRedisGenerator.java 
* @Package com.beaconem.common.redis.service.generator
* @Description: TODO(用一句话描述该文件做什么) 
* @author hb8588  
* @date 2016年11月15日 上午11:02:47 
* @version V1.0   
*/
package com.ideal.manage.cache.generator;

import com.ideal.manage.cache.generator.IdGeneratorCommon.IdGeneratorBuilder;
import org.apache.commons.lang3.StringUtils;
import redis.clients.jedis.JedisPoolConfig;

import java.util.List;

/**
 * @ClassName: JRedisGenerator
 * @Description: TODO(这里用一句话描述这个类的作用)
 * @author hb8588
 * @date 2016年11月15日 上午11:02:47
 * 
 */
public class JRedisGenerator {

	private String clusterIds = "172.29.10.189:6379:d6484e158be890dac21b51d3615fc0f9088e1683";
	private IdGeneratorCommon idGenerator;

	private int maxIdle = 5;
	private int maxTotal = 50;
	private int minIdle = 5;
	private int maxWaitMillis = 10000;
	private boolean isBorrow = true;

	/**
	 * @param clusterIds
	 */
	public void setClusterIds(String clusterIds) {
		this.clusterIds = clusterIds;
	}

	public void init() {
		if (StringUtils.isNotBlank(this.clusterIds)) {
			String[] ipArr = clusterIds.split(",");
			IdGeneratorBuilder builder = IdGeneratorCommon.builder();
			String host = "";
			int port = 6379;
			String luaSha = "";

			JedisPoolConfig config = new JedisPoolConfig();
			config.setMaxIdle(this.maxIdle);
			config.setMaxTotal(this.maxTotal);
			config.setMinIdle(this.minIdle);
			config.setMaxWaitMillis((long)this.maxWaitMillis);
			config.setTestOnBorrow(this.isBorrow);
			config.setTestOnReturn(true);

			for (String ipPort : ipArr) {
				String[] hostPort = ipPort.split(":");
				host = hostPort[0];
				port = Integer.parseInt(hostPort[1]);
				luaSha = hostPort[2];
				builder.addHost(config, host, port, luaSha);
			}
			idGenerator = builder.build();
		}
	}

	/**
	 * 
	 * @Title: getId;
	 * @Description: 获取唯一ID
	 * @param @param key
	 * @param @param value
	 * @param @return;
	 * @return long; @throws;
	 */
	public long getId(String key, long value) {
		return idGenerator.next(key, value);
	}

	/**
	 * 
	 * @Title: getId;
	 * @Description: 获取唯一ID
	 * @param @param key
	 * @param @param value
	 * @param @return;
	 * @return long; @throws;
	 */
	public List<Long> getId(String key, List<Long> value) {
		return idGenerator.next(key, value);
	}

	/**
	 * 
	 * @Title: getGeneratorModel;
	 * @Description: 根据唯一ID拆分
	 * @param @param id
	 * @param @return;
	 * @return GeneratorModel; @throws;
	 */
	public GeneratorModel getGeneratorModel(long id) {
		List<Long> result = IdGeneratorCommon.parseId(id);
		return new GeneratorModel(result.get(0), result.get(1), result.get(2));
	}

	public void setMaxIdle(int maxIdle) {
		this.maxIdle = maxIdle;
	}

	public void setMaxTotal(int maxTotal) {
		this.maxTotal = maxTotal;
	}

	public void setMinIdle(int minIdle) {
		this.minIdle = minIdle;
	}

	public void setMaxWaitMillis(int maxWaitMillis) {
		this.maxWaitMillis = maxWaitMillis;
	}

	public void setBorrow(boolean borrow) {
		isBorrow = borrow;
	}
}

class GeneratorModel {
	private long miliSeconds;
	private long partition;
	private long seq;

	/**
	 * @Title: @Description: TODO(这里用一句话描述这个方法的作用) @author:hb8588 @param miliSeconds @param partition @param seq @return @throws
	 */
	public GeneratorModel(long miliSeconds, long partition, long seq) {
		super();
		this.miliSeconds = miliSeconds;
		this.partition = partition;
		this.seq = seq;
	}

	/**
	 * @return miliSeconds
	 */
	public long getMiliSeconds() {
		return miliSeconds;
	}

	/**
	 * @param miliSeconds
	 */
	public void setMiliSeconds(long miliSeconds) {
		this.miliSeconds = miliSeconds;
	}

	/**
	 * @return partition
	 */
	public long getPartition() {
		return partition;
	}

	/**
	 * @param partition
	 */
	public void setPartition(long partition) {
		this.partition = partition;
	}

	/**
	 * @return seq
	 */
	public long getSeq() {
		return seq;
	}

	/**
	 * @param seq
	 */
	public void setSeq(long seq) {
		this.seq = seq;
	}

}