/**   
* @Title: CommonConstants.java 
* @Package com.beaconem.iot.common.config
* @Description: TODO(用一句话描述该文件做什么) 
* @author hb8588  
* @date 2016年9月22日 下午5:18:06 
* @version V1.0   
*/
package com.ideal.manage.cache.config;

/**
 * @ClassName: CommonConstants
 * @Description: TODO(这里用一句话描述这个类的作用)
 * @author hb8588
 * @date 2016年9月22日 下午5:18:06
 * 
 */
public class CommonConstants {
	// REDIS的serverIP信息
	public static final String SERVER_REDIS_HSET_NAME = "SERVER_LIST";
	// REDIS的serverINCR信息
	public static final String SERVER_REDIS_HSET_NAME_INCR = "SERVER_ID_INCR";
	// REDIS的网关服务信息
	public static final String GATEWAY_SERVER_IP_KEY_NAME = "IP";
	// REDIS的网关ID信息
	public static final String GATEWAY_REQUEST_ID_KEY_NAME = "ID";
	// REDIS的网关服务信息超时时间
	public static final int GATEWAY_SERVER_OUT_TIME = 60;
	
	public static final String HANDER_NAME = "FLOWINFO";

}
