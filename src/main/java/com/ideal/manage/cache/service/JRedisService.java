/**   
* @Title: ReidsService.java 
* @Package com.beaconem.iot.broker.service.redis
* @Description: TODO(用一句话描述该文件做什么) 
* @author hb8588  
* @date 2016年9月22日 下午4:09:28 
* @version V1.0   
*/
package com.ideal.manage.cache.service;

import redis.clients.jedis.Tuple;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @ClassName: ReidsService
 * @Description: TODO(这里用一句话描述这个类的作用)
 * @author hb8588
 * @date 2016年9月22日 下午4:09:28
 * 
 */
public interface JRedisService {

	/**
	 * 
	 * @Title: init;
	 * @Description: 初始化服务信息
	 * @param ;
	 * @return void; @throws;
	 */
	public void init();

	/**
	 * 
	 * @Title: getServerList;
	 * @Description: 获取服务IP地址列表;
	 * @param @param deviceId
	 * @param @return;
	 * @return Map<String,String>; @throws;
	 */
	public Map<String, String> getServerList();

	/**
	 * key-value加入缓存
	 * 
	 * @param key
	 * @param value
	 * @return 添加是否成功
	 */
	boolean add(String key, Object value);


	/**
	 * key-value加入缓存
	 * 
	 * @param key
	 * @param value
	 * @param expiry 缓存时间
	 * @return 添加是否成功
	 */
	boolean add(String key, Object value, long expiry);
	
	/**
	 * key-value加入缓存
	 * 
	 * @param key
	 * @param value
	 * @param expiry 缓存时间
	 * @return 添加是否成功
	 */
	boolean add(String key, String value, long expiry);
	/**
	 * key-value加入缓存，但是KEY必须是不存在
	 * 
	 * @param key
	 * @param value
	 * @param expiry 缓存时间
	 * @return 添加是否成功
	 */
	boolean addnx(String key, String value, int expiry);
	
	/**
	 * key-value(String)加入缓存
	 *
	 * @param key
	 * @param value
	 * @return 添加是否成功
	 */
	boolean addString(String key, String value);
	/**
	 * key-value(String)加入缓存
	 *
	 * @param key
	 * @param value
	 * @param expiry 缓存时间
	 * @return 添加是否成功
	 */
	boolean addString(String key, String value, long expiry);

	/**
	 * 通过key，得到value
	 * 
	 * @param key
	 * @return 取出缓存中的对象
	 */
	String get(String key);

	Object getObject(String key);

	/**
	 * 通过key进行删除
	 * 
	 * @param key
	 * @return 删除返回结果
	 */
	Long del(String key);

	/**
	 * 
	* @Title: incr; 
	* @Description: 通过key进行获取自增长
	* @param @param key
	* @param @return;
	* @return Long;
	* @throws;
	 */
	Long incr(String key);
	
	/**
	 * 
	* @Title: incrBy; 
	* @Description: 通过key进行获取自增长
	* @param @param key
	* @param @return;
	* @return Long;
	* @throws;
	 */
	Long incrBy(String key, long value);
	/**
	 * 
	 * @Title: expire;
	 * @Description: 设置失效时间
	 * @param @param key
	 * @param @param seconds
	 * @param @return;
	 * @return Long; @throws;
	 */
	Long expire(String key, int seconds);

	/**
	 * 
	 * @Title: ttl;
	 * @Description: 获取失效时间
	 * @param @param key
	 * @param @param seconds
	 * @param @return;
	 * @return Long; @throws;
	 */
	Long ttl(String key);

	/**
	 * 
	 * @Title: expireAndTTL;
	 * @Description: 设置失效如果有失效时间返回失效时间
	 * @param @param key
	 * @param @param seconds
	 * @param @return;
	 * @return Long; @throws;
	 */
	Long expireAndTTL(String key, int seconds);

	/**
	 * @Title: addHash;
	 * @Description: TODO(这里用一句话描述这个方法的作用);
	 * @param @param key
	 * @param @param field
	 * @param @param value
	 * @param @param expiry
	 * @param @return;
	 * @return boolean; @throws;
	 */
	boolean addHash(String key, String field, String value, int expiry);

	/**
	 * @Title: addHash;
	 * @Description: TODO(这里用一句话描述这个方法的作用);
	 * @param @param key
	 * @param @param hash
	 * @param @param expiry
	 * @param @return;
	 * @return boolean; @throws;
	 */
	boolean addHash(String key, Map<String, String> hash, int expiry);

	/**
	 * @Title: addHash;
	 * @Description: TODO(这里用一句话描述这个方法的作用);
	 * @param @param key
	 * @param @param hash
	 * @param @return;
	 * @return boolean; @throws;
	 */
	boolean addHash(String key, Map<String, String> hash);

	/**
	 * delHash
	 * 
	 * @param key
	 * @param field
	 * @return
	 */
	Long delHash(String key, String... field);

	/**
	 * 
	 * @Title: delHashAndExp;
	 * @Description: 删除键值与失效KEY时长
	 * @param @param key
	 * @param @param expiry
	 * @param @param field
	 * @param @return;
	 * @return Long; @throws;
	 */
	Long delHashAndExp(String key, int expiry, String... field);

	/**
	 * @Title: getHash;
	 * @Description: TODO(这里用一句话描述这个方法的作用);
	 * @param @param key
	 * @param @param field
	 * @param @return;
	 * @return String; @throws;
	 */
	String getHash(String key, String field);

	/**
	 * @Title: getHashList;
	 * @Description: TODO(这里用一句话描述这个方法的作用);
	 * @param @param key
	 * @param @param fields
	 * @param @return;
	 * @return List<String>; @throws;
	 */
	List<String> getHashList(String key, String... fields);

	/**
	 * @Title: exists;
	 * @Description: TODO(这里用一句话描述这个方法的作用);
	 * @param @param key
	 * @param @return;
	 * @return boolean; @throws;
	 */
	boolean exists(String key);

	/**
	 * @Title: existsHash;
	 * @Description: TODO(这里用一句话描述这个方法的作用);
	 * @param @param key
	 * @param @param field
	 * @param @return;
	 * @return boolean; @throws;
	 */
	boolean existsHash(String key, String field);

	/**
	 * 
	 * @Title: addHashNX;
	 * @Description: 添加Hash开始与结束
	 * @param @param key
	 * @param @param startField
	 * @param @param startValue
	 * @param @param endField
	 * @param @param endValue
	 * @param @return;
	 * @return boolean; @throws;
	 */
	String addHashNX(String key, String startField, String startValue, String endField, String endValue, int seconds);

	/**
	 * 
	 * @Title: addHashNXDel;
	 * @Description: 删除添加新
	 * @param @param key
	 * @param @param delField
	 * @param @param field
	 * @param @return;
	 * @return boolean; @throws;
	 */
	boolean addHashNXDel(String key, String delField, String field, String value);

	/**
	 * 
	 * @Title: addHashNX;
	 * @Description: 添加hash是否存在
	 * @param @param key
	 * @param @param field
	 * @param @param value
	 * @param @return;
	 * @return boolean; @throws;
	 */
	boolean addHashNX(String key, String field, String value);

	/**
	 * 
	 * @Title: addHashNX;
	 * @Description: 添加hash是否存在
	 * @param @param key
	 * @param @param field
	 * @param @param value
	 * @param @return;
	 * @return boolean; @throws;
	 */
	String addHashNXGet(String key, String field, String value);

	/**
	 * 
	 * @Title: getHashNXGet;
	 * @Description: 添加hash是否存在
	 * @param @param key
	 * @param @param field
	 * @param @param value
	 * @param @return;
	 * @return boolean; @throws;
	 */
	Map<String, String> getHashNXGet(String key, String field, String value);
	
	/**
	 * @Title: getHashAll;
	 * @Description: TODO(这里用一句话描述这个方法的作用);
	 * @param @param key
	 * @param @return;
	 * @return Map<String,String>; @throws;
	 */
	Map<String, String> getHashAll(String key);
	
	/**
	 * 
	* @Title: getHashLen; 
	* @Description: TODO(这里用一句话描述这个方法的作用); 
	* @param @param key
	* @param @return;
	* @return long;
	* @throws;
	 */
	long getHashLen(String key);

	/**
	 * @Title: addSet;
	 * @Description: TODO(这里用一句话描述这个方法的作用);
	 * @param @param key
	 * @param @param hash
	 * @param @return;
	 * @return boolean; @throws;
	 */
	boolean addSet(String key, Map<String, Double> hash);

	boolean addSet(String key, Map<String, Double> hash, int expiry);
	/**
	 * @Title: getSetAll;
	 * @Description: TODO(这里用一句话描述这个方法的作用);
	 * @param @param key
	 * @param @return;
	 * @return Set<String>; @throws;
	 */
	Set<String> getSetAll(String key);

	Set<Tuple> getSetAllWithScores(String key);

	/**
	 * 
	 * @Title: get;
	 * @Description: 取出缓存中的对象
	 * @param @param key
	 * @param @param clazz
	 * @param @return;
	 * @return T; @throws;
	 */
	<T> T get(String key, Class<T> clazz);

	/**
	 * 
	 * @Title: zincrBy;
	 * @Description: 有序集合存储增量键比;
	 * @param @param key
	 * @param @param member
	 * @param @return;
	 * @return double; @throws;
	 */
	double zincrBy(String key, String member);

	/**
	 * 
	 * @Title: zrange;
	 * @Description: 获取集合的数据
	 * @param @param key
	 * @param @param start
	 * @param @param end
	 * @param @return;
	 * @return Set<String>; @throws;
	 */
	Set<String> zrange(String key, long start, long end);

	/**
	 * @param @param   key
	 * @param @param   start
	 * @param @param   end
	 * @param @return;
	 * @return Set<String>; @throws;
	 * @Title: zrange;
	 * @Description: 获取集合的数据
	 */
	Set<Tuple> zrangeWithScores(String key, long start, long end);

	/**
	 * 
	 * @Title: zrem;
	 * @Description: 有序集合存储在键删除指定成员
	 * @param @param key
	 * @param @param member
	 * @param @return;
	 * @return long; @throws;
	 */
	long zrem(String key, String... member);

	/**
	 * 
	 * @Title: addHash;
	 * @Description: 添加key值
	 * @param @param key
	 * @param @param field
	 * @param @param value
	 * @param @return;
	 * @return boolean; @throws;
	 */
	boolean addHash(String key, String field, String value);

	/**
	 * 
	 * @Title: pushOrRem;
	 * @Description: 去重复的队列
	 * @param @param key
	 * @param @param value
	 * @param @param ishead
	 * @param @return;
	 * @return double; @throws;
	 */
	double pushOrRem(String key, String value, boolean ishead);

	/**
	 * 
	 * @Title: push;
	 * @Description:添加队列
	 * @param @param key
	 * @param @param value
	 * @param @return;
	 * @return double; @throws;
	 */
	double push(String key, String value, boolean ishead);

	/**
	 *
	 * @Title: push;
	 * @Description:支持多个值添加
	 * @param @param key
	 * @param @param value
	 * @param @return;
	 * @return double; @throws;
	 */
	double push(String key, boolean ishead, List<String> values);

	/**
	 * 
	 * @Title: push;
	 * @Description: TODO(这里用一句话描述这个方法的作用);
	 * @param @param key
	 * @param @param ishead
	 * @param @return;
	 * @return String; @throws;
	 */
	String pop(String key, boolean ishead);

	/**
	 *
	 * @Title: lrange;
	 * @Description: TODO(这里用一句话描述这个方法的作用);
	 * @param @param key
	 * @param @return;
	 * @return List<String>; @throws;
	 */
	List<String> lrange(String key, long start, long end);

	/**
	 * 
	 * @Title: popOrRem;
	 * @Description: 弹出一个值同时移除想通值
	 * @param @param key
	 * @param @param isHead
	 * @param @return;
	 * @return String; @throws;
	 */
	String popOrRem(String key, boolean isHead);
	
	/**
	 * 
	* @Title: listLen; 
	* @Description: 获取列表的长度
	* @param @param key
	* @param @return;
	* @return long;
	* @throws;
	 */
	long listLen(String key);

	/**
	 *
	 * @Title: hIncrBy;
	 * @Description: 递增在键值字段中存储的值
	 * @param @param key
	 * @param @param field
	 * @param @param value
	 * @param @return;
	 * @return String; @throws;
	 */
	Long hIncrBy(String key, String field, long value);
	
	/**
	 *
	 * @Title: hIncrBy;
	 * @Description: 递增在键值字段中存储的值
	 * @param @param key
	 * @param @param field
	 * @param @param value
	 * @param @return;
	 * @return String; @throws;
	 */
	Long hIncrBy(String key, Map<String, Long> map, int seconds);
	
	/**
	 *
	 * @Title: zIncrBy;
	 * @Description: 递增在键值字段中存储的值
	 * @param @param key
	 * @param @param field
	 * @param @param value
	 * @param @return;
	 * @return String; @throws;
	 */
	Double zIncrBy(String key, String field, long value);
}
