package com.ideal.manage.cache.service.project;

import com.ideal.manage.cache.service.CacheService;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by Administrator on 2019/1/26 0026.
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class CacheServiceTest {

    private Logger logger = LoggerFactory.getLogger(com.ideal.manage.cache.service.CacheService.class);

    @Autowired
    private CacheService cacheService;

    @Before
    public void setUp() throws Exception {

    }

    @After
    public void tearDown() throws Exception {

    }

    @Test
    public void testFindAll() throws Exception {

    }

    @Test
    public void testFindAll1() throws Exception {

    }

    @Test
    public void testFindById() throws Exception {

    }

    /**
     * 单元测试回滚：单元个测试的时候如果不想造成垃圾数据，可以开启事物功能，记在方法或者类头部添加@Transactional注解即可
     * @throws Exception
     */
    @Test
    public void testSaveProject() throws Exception {
        cacheService.set("mess","kafka");
    }

    @Test
    public void testDeleteProject() throws Exception {

    }
}