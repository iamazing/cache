package com.ideal.manage.cache.util;


/**
 * Created by Administrator on 2019/1/24 0024.
 */
public class Const {

    public static final String BOOT_CACHE = "BOOT_CACHE";

    public static final String USERNAME_CACHE = "USERNAME_CACHE";


    /**
     * 缓存时间设置为10s
     */
    public static final Long EXPIRE_TIME_SECONDS = 10L;


    public static final String CACHE_HEADER = "cache:";
    public static final String CACHE_REPORT = "report:";
    public static final String CACHE_ALARM = "alarm:";

    public static String USERNAME_IN_CONTEXT;
}
