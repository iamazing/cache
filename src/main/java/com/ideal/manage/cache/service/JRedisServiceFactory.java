/**   
* @Title: JRedisServiceFactory.java 
* @Package com.beaconem.iot.broker.service.redis
* @Description: TODO(用一句话描述该文件做什么) 
* @author hb8588  
* @date 2016年9月26日 上午9:28:48 
* @version V1.0   
*/
package com.ideal.manage.cache.service;

import java.io.Serializable;

/**
 * @ClassName: JRedisServiceFactory
 * @Description: TODO(这里用一句话描述这个类的作用)
 * @author hb8588
 * @date 2016年9月26日 上午9:28:48
 * 
 */
public class JRedisServiceFactory implements Serializable {

	private JRedisService jRedisService;

	private JRedisService jRedisClusterService;

	private boolean isCluster = true;

	private static JRedisService single = null;

	/**
	 * 
	 * @Title: init;
	 * @Description: 初始化服务信息
	 * @param ;
	 * @return void; @throws;
	 */
	public void init() {
		if (this.isCluster)
			single = jRedisClusterService;
		else 
			single = jRedisService;
		single.init();
	}

	public JRedisService getJRedisService() {
		return single;
	}

	/**
	 * @param jRedisService
	 */
	public void setjRedisService(JRedisService jRedisService) {
		this.jRedisService = jRedisService;
	}

	/**
	 * @param jRedisClusterService
	 */
	public void setjRedisClusterService(JRedisService jRedisClusterService) {
		this.jRedisClusterService = jRedisClusterService;
	}

	/**
	 * @param isCluster
	 */
	public void setCluster(boolean isCluster) {
		this.isCluster = isCluster;
	}

}
