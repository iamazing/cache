/**   
* @Title: IdGenerator.java 
* @Package com.beaconem.common.redis.service.impl
* @Description: TODO(用一句话描述该文件做什么) 
* @author hb8588  
* @date 2016年11月15日 上午10:04:25 
* @version V1.0   
*/
package com.ideal.manage.cache.generator;

/** 
* @ClassName: IdGenerator 
* @Description: TODO(这里用一句话描述这个类的作用) 
* @author hb8588 
* @date 2016年11月15日 上午10:04:25 
*  
*/
import org.apache.commons.lang3.tuple.Pair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;
import redis.clients.jedis.exceptions.JedisConnectionException;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class IdGeneratorCommon {

	static final Logger logger = LoggerFactory.getLogger(IdGeneratorCommon.class);
	/**
	 * JedisPool, luaSha
	 */
	List<Pair<JedisPool, String>> jedisPoolList;
	int retryTimes;

	int index = 0;

	private IdGeneratorCommon() {

	}

	private IdGeneratorCommon(List<Pair<JedisPool, String>> jedisPoolList, int retryTimes) {
		this.jedisPoolList = jedisPoolList;
		this.retryTimes = retryTimes;
	}

	static public IdGeneratorBuilder builder() {
		return new IdGeneratorBuilder();
	}

	static class IdGeneratorBuilder {
		List<Pair<JedisPool, String>> jedisPoolList = new ArrayList<Pair<JedisPool, String>>();
		int retryTimes = 5;

		public IdGeneratorBuilder addHost(JedisPoolConfig jedisPoolConfig, String host, int port, String luaSha) {
			jedisPoolList.add(Pair.of(new JedisPool(jedisPoolConfig, host, port), luaSha));
			return this;
		}

		public IdGeneratorBuilder retryTimes(int retryTimes) {
			this.retryTimes = retryTimes;
			return this;
		}

		public IdGeneratorCommon build() {
			return new IdGeneratorCommon(jedisPoolList, retryTimes);
		}
	}

	public long next(String tab) {
		return next(tab, 0);
	}

	public long next(String tab, long shardId) {
		for (int i = 0; i < retryTimes; ++i) {
			Long id = innerNext(tab, shardId);
			if (id != null) {
				return id;
			}
		}
		throw new RuntimeException("Can not generate id!");
	}

	public List<Long> next(String tab, List<Long> shardId) {
		return innerNext(tab, shardId);
	}

	Long innerNext(String tab, long shardId) {
		index++;
		Pair<JedisPool, String> pair = jedisPoolList.get(index % jedisPoolList.size());
		JedisPool jedisPool = pair.getLeft();

		String luaSha = pair.getRight();
		Jedis jedis = null;
		try {
			jedis = jedisPool.getResource();
			List<Long> result = (List<Long>) jedis.evalsha(luaSha, 2, tab, String.valueOf(shardId));
			long id = buildId(result.get(0), result.get(1), result.get(2), result.get(3));
			return id;
		} catch (JedisConnectionException e) {
			logger.error("generate id error!", e);
		} finally {
			if (jedis != null) {
				jedis.close();
			}
		}
		return null;
	}

	List<Long> innerNext(String tab, List<Long> shardIds) {
		index++;
		Pair<JedisPool, String> pair = jedisPoolList.get(index % jedisPoolList.size());
		JedisPool jedisPool = pair.getLeft();
		List<Long> list = new LinkedList<Long>();
		String luaSha = pair.getRight();
		Jedis jedis = null;
		try {
			jedis = jedisPool.getResource();
			for (Long shardId : shardIds) {
				List<Long> result = (List<Long>) jedis.evalsha(luaSha, 2, tab, String.valueOf(shardId));
				Long id = buildId(result.get(0), result.get(1), result.get(2), result.get(3));
				if (id != null) {
					list.add(id);
					continue;
				}
				list.add(null);
			}
			return list;
		} catch (JedisConnectionException e) {
			logger.error("generate id error!", e);
		} finally {
			if (jedis != null) {
				jedis.close();
			}
		}
		return list;
	}

	public static long buildId(long second, long microSecond, long shardId, long seq) {
		long miliSecond = (second * 1000 + microSecond / 1000);
		return (miliSecond << (12 + 10)) + (shardId << 10) + seq;
	}

	public static List<Long> parseId(long id) {
		long miliSecond = id >>> 22;
		// 2 ^ 12 = 0xFFF
		long shardId = (id & (0xFFF << 10)) >> 10;
		long seq = id & 0x3FF;

		List<Long> re = new ArrayList<Long>(4);
		re.add(miliSecond);
		re.add(shardId);
		re.add(seq);
		return re;
	}
}