package com.ideal.manage.cache.service.impl;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import redis.clients.jedis.*;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by hb on 2016/9/17.
 */
public class JedisClusterClient implements Serializable {

	private static Logger log = LoggerFactory.getLogger(JedisClusterClient.class);

	private JedisCluster jedisCluster = null;
	private ShardedJedisPool shardedJedisPool;// 切片连接池
	private String clusters = "172.29.12.17:6379,172.29.12.18:6379,172.29.12.19:6379,172.29.12.17:6378,172.29.12.18:6378,172.29.12.19:6378";
	private int maxIdle = 5;
	private int maxTotal = 50;
	private int minIdle = 5;
	private int maxWaitMillis = 10000;
	private boolean isBorrow = true;
	private boolean isCluster = true;
	private boolean isReturn = true;
	private String password = "";

	
	/**  
	* @Title:
	* @Description: TODO(这里用一句话描述这个方法的作用)  
	* @author:hb
	* @return 
	* @throws  
	*/
	public JedisClusterClient() {
		super();
	}

	/**  
	* @Title:
	* @Description: TODO(这里用一句话描述这个方法的作用)  
	* @author:hb
	* @param jedisCluster
	* @param shardedJedisPool
	* @param clusters
	* @param maxIdle
	* @param maxTotal
	* @param minIdle
	* @param maxWaitMillis
	* @param isBorrow
	* @param isCluster
	* @param isReturn
	* @param password
	* @return 
	* @throws  
	*/
	public JedisClusterClient(String clusters, int maxIdle, int maxTotal, int minIdle, int maxWaitMillis, boolean isBorrow, boolean isCluster, boolean isReturn, String password) {
		super();
		this.clusters = clusters;
		this.maxIdle = maxIdle;
		this.maxTotal = maxTotal;
		this.minIdle = minIdle;
		this.maxWaitMillis = maxWaitMillis;
		this.isBorrow = isBorrow;
		this.isCluster = isCluster;
		this.isReturn = isReturn;
		this.password = password;
	}

	public void initCluster() {
		try {
			Set<HostAndPort> jedisClusterNodes = new HashSet<>();
			String host = "";
			int port = 6379;
			String[] ipArr = clusters.split(",");
			for (String ipPort : ipArr) {
				String[] hostPort = ipPort.split(":");
				host = hostPort[0];
				port = Integer.parseInt(hostPort[1]);
				jedisClusterNodes.add(new HostAndPort(host, port));
			}
			JedisPoolConfig config = new JedisPoolConfig();
			config.setMaxIdle(this.maxIdle);
			config.setMaxTotal(this.maxTotal);
			config.setMinIdle(this.minIdle);
			config.setMaxWaitMillis(this.maxWaitMillis);
			config.setTestOnBorrow(this.isBorrow);
			config.setTestOnReturn(this.isReturn);
			jedisCluster = new JedisCluster(jedisClusterNodes, config);
		} catch (Exception e) {
			e.printStackTrace();
			log.error(e.getMessage());
		}
	}

	public void initNotCluster() {
		try {
			String host = "";
			int port = 6379;
			String[] ipArr = clusters.split(",");
			// slave链接
			List<JedisShardInfo> shards = new ArrayList<JedisShardInfo>();
			for (String ipPort : ipArr) {
				String[] hostPort = ipPort.split(":");
				JedisShardInfo info = new JedisShardInfo(hostPort[0], Integer.parseInt(hostPort[1]), hostPort[0].replace(".", ""));
				info.setPassword(this.password);
				shards.add(info);
			}
			JedisPoolConfig config = new JedisPoolConfig();
			config.setMaxIdle(this.maxIdle);
			config.setMaxTotal(this.maxTotal);
			config.setMinIdle(this.minIdle);
			config.setMaxWaitMillis(this.maxWaitMillis);
			config.setTestOnBorrow(this.isBorrow);
			config.setTestOnReturn(false);
			log.info("{},{},{}", host, port, password);
			shardedJedisPool = new ShardedJedisPool(config, shards);
		} catch (Exception e) {
			e.printStackTrace();
			log.error(e.getMessage());
		}
	}

	public void destroy() {
		if (jedisCluster != null) {
			try {
				jedisCluster.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		if (shardedJedisPool != null) {
			shardedJedisPool.close();
		}
	}

	public JedisCluster getJedisCluster() {
		if (this.jedisCluster == null) {
			synchronized (JedisClusterClient.class) {
				if (this.jedisCluster == null) {
					initCluster();
				}
			}
		}
		return jedisCluster;
	}

	public ShardedJedisPool getShardedJedisPool() {
		if (this.shardedJedisPool == null && !this.isCluster) {
			synchronized (JedisClusterClient.class) {
				if (this.shardedJedisPool == null) {
					initNotCluster();
				}
			}
		}
		return shardedJedisPool;
	}

	public ShardedJedis getJedisResource() {
		return getShardedJedisPool().getResource();
	}

	/**
	 * @param cluster_ip_ports
	 */
	public void setClusters(String clusters) {
		this.clusters = clusters;
	}

	/**
	 * @param maxIdle
	 */
	public void setMaxIdle(int maxIdle) {
		this.maxIdle = maxIdle;
	}

	/**
	 * @param maxTotal
	 */
	public void setMaxTotal(int maxTotal) {
		this.maxTotal = maxTotal;
	}

	/**
	 * @param minIdle
	 */
	public void setMinIdle(int minIdle) {
		this.minIdle = minIdle;
	}

	/**
	 * @param maxWaitMillis
	 */
	public void setMaxWaitMillis(int maxWaitMillis) {
		this.maxWaitMillis = maxWaitMillis;
	}

	/**
	 * @param isBorrow
	 */
	public void setBorrow(boolean isBorrow) {
		this.isBorrow = isBorrow;
	}

	/**
	 * @param password
	 */
	public void setPassword(String password) {
		this.password = password;
		if (StringUtils.isEmpty(password))
			this.password = null;
	}

	/**
	 * @param isCluster
	 */
	public void setCluster(boolean isCluster) {
		this.isCluster = isCluster;
	}

	/**
	 * @param isReturn
	 */
	public void setReturn(boolean isReturn) {
		this.isReturn = isReturn;
	}
}
