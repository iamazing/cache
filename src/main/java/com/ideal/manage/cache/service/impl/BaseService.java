/**   
* @Title: BaseService.java 
* @Package com.beaconem.iot.redis.service.impl
* @Description: TODO(用一句话描述该文件做什么) 
* @author hb8588  
* @date 2016年9月30日 下午5:29:56 
* @version V1.0   
*/
package com.ideal.manage.cache.service.impl;

import com.ideal.manage.cache.util.CommonUitl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Serializable;

/**
 * @ClassName: BaseService
 * @Description: TODO(这里用一句话描述这个类的作用)
 * @author hb8588
 * @date 2016年9月30日 下午5:29:56
 * 
 */
public class BaseService implements Serializable {

	final Logger logger = LoggerFactory.getLogger(this.getClass());
	// 设置ID
	String serverId = "1";

	// 本机IP
	String localAddress = CommonUitl.LOCAL_IP_ADDRESS;

	// 启动端口
	String port = "";
	// RedisClient
	JedisClusterClient jedisClusterClient;

	String getLocalIpPort() {
		return this.localAddress + ":" + port;
	}

	/**
	 * @param serverId
	 */
	public void setServerId(String serverId) {
		this.serverId = serverId;
	}

	/**
	 * @param jedisClusterClient
	 */
	public void setJedisClusterClient(JedisClusterClient jedisClusterClient) {
		this.jedisClusterClient = jedisClusterClient;
	}

	/**
	 * @return localAddress
	 */
	public String getLocalAddress() {
		return localAddress;
	}

	/**
	 * @param localAddress
	 */
	public void setLocalAddress(String localAddress) {
		this.localAddress = localAddress;
	}

	/**
	 * @param port
	 */
	public void setPort(String port) {
		this.port = port;
	}
}
