package com.ideal.manage.cache.service;

import com.ideal.manage.cache.util.RedisUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Administrator
 * @date 2019/3/7 0007
 */
@Service
public class CacheService {

    @Autowired
    private RedisUtil redisUtil;

    public boolean set(String key, String value){
        redisUtil.set(key,value);
        return true;
    }
}
