/**   
* @Title: JRedisServerServiceImpl.java 
* @Package com.beaconem.iot.redis.service.impl
* @Description: TODO(用一句话描述该文件做什么) 
* @author hb8588  
* @date 2016年9月26日 下午12:45:57 
* @version V1.0   
*/
package com.ideal.manage.cache.service.impl;

import com.ideal.manage.cache.service.JRedisServerService;
import com.ideal.manage.cache.service.JRedisServiceFactory;
import org.apache.commons.lang3.StringUtils;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

/**
 * @ClassName: JRedisServerServiceImpl
 * @Description: TODO(这里用一句话描述这个类的作用)
 * @author hb8588
 * @date 2016年9月26日 下午12:45:57
 * 
 */
public class JRedisServerServiceImpl implements JRedisServerService {
	
	private static ConcurrentMap<String, String> serverMap = new ConcurrentHashMap<>();

	private JRedisServiceFactory jRedisServiceFactory;

	public void init() {
		Map<String, String> map = jRedisServiceFactory.getJRedisService().getServerList();
		if (map != null) {
			for (Map.Entry<String, String> entry : map.entrySet()) {
				serverMap.put(entry.getValue(), entry.getKey());
			}
		}
	}

	/**
	 * 
	 * @Title: getServerIP;
	 * @Description: 获取服务IP地址;
	 * @param @param serverId
	 * @param @return;
	 * @return String; @throws;
	 */
	public String getServerIP(String serverId) {
		String ip = serverMap.get(serverId);
		if (StringUtils.isEmpty(ip)) {
			synchronized (JRedisServerServiceImpl.class) {
				ip = serverMap.get(serverId);
				if (StringUtils.isEmpty(ip)) {
					init();
				}
			}
		}
		return ip;
	}

	/**
	 * @param jRedisServiceFactory
	 */
	public void setjRedisServiceFactory(JRedisServiceFactory jRedisServiceFactory) {
		this.jRedisServiceFactory = jRedisServiceFactory;
	}
}
