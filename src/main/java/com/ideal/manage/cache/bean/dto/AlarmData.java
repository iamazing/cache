package com.ideal.manage.cache.bean.dto;

import java.io.Serializable;

/**
 * Desc:    报警数据
 * Author: Iron
 * CreateDate: 2016-11-14
 * CopyRight: Beijing Yunzong Co., Ltd.
 */
public class AlarmData implements Serializable{

    private String key;
    private String value;

    private long time;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }

    @Override
    public String toString() {
        return "AlarmData{" +
                "key='" + key + '\'' +
                ", value='" + value + '\'' +
                ", time=" + time +
                '}';
    }
}
