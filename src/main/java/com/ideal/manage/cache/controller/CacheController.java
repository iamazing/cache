package com.ideal.manage.cache.controller;

import com.ideal.manage.cache.bean.dto.AlarmData;
import com.ideal.manage.cache.bean.dto.MSG;
import com.ideal.manage.cache.util.Const;
import com.ideal.manage.cache.util.LogMessage;
import com.ideal.manage.cache.util.RedisUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

/**
 * @author yaming
 * @date 2019-03-06
 */
@Controller
@RequestMapping("cache")
public class CacheController {

    @Autowired
    private RedisUtil redisUtil;

    private Logger logger = LoggerFactory.getLogger(CacheController.class);

    /**
     * 缓存--放入缓存
     * @param key
     * @param value
     * @return
     */
    @RequestMapping(value = "set",method = RequestMethod.POST)
    @ResponseBody
    public MSG set(@RequestParam(value = "key") String key, @RequestParam(value = "value") String value){

        String cacheKey = Const.CACHE_HEADER + key;
        boolean flag = redisUtil.set(cacheKey,value);

        logger.info("cache,put into redis{" + LogMessage.out(flag) + "}");
        MSG msg = new MSG();
        if (flag == true){
            msg.setStatus("200");
        }
        return msg;
    }

    /**
     * 带超时时间的缓存---报警信息
     * @param alarmData
     * @return
     */
    @RequestMapping(value = "set/expire",method = RequestMethod.POST,consumes="application/json")
    @ResponseBody
    public MSG set(@RequestBody AlarmData alarmData){
        String cacheKey = Const.CACHE_HEADER + alarmData.getKey();
        boolean flag = redisUtil.set(cacheKey,alarmData.getValue(), alarmData.getTime());

        logger.info("cache,put into redis with expire{" + LogMessage.out(flag) + "}");
        MSG msg = new MSG();
        if (flag == true){
            msg.setStatus("200");
        }
        return msg;
    }


    /**
     * 缓存--根据key查询
     * @param key
     * @return
     */
    @RequestMapping(value = "get",method = RequestMethod.GET)
    @ResponseBody
    public MSG get(@RequestParam(value = "key") String key){

        //String cacheKey = Const.CACHE_HEADER + key;
        String cacheKey = key;
        Object value = redisUtil.get(cacheKey);
        logger.info("cache,get from redis,key:"+cacheKey+",value"+value.toString());
        MSG msg = new MSG("200",value);
        return msg;
    }

    /**
     * 判断key是否存在--判断告警信息是否过期
     * @param alarmData
     * @return
     */
    @RequestMapping(value = "has",method = RequestMethod.POST,consumes="application/json")
    @ResponseBody
    public MSG hasKey(@RequestBody AlarmData alarmData){

        String cacheKey = Const.CACHE_HEADER + alarmData.getKey();
        boolean flag = redisUtil.hasKey(cacheKey);

        logger.info("cache,get from redis{" + LogMessage.out(flag) + "}" + "hasKey:"+cacheKey);

        MSG msg = new MSG();
        if (flag == true){
            msg.setStatus("200");
        }
        return msg;
    }

    /**
     * incr加1，做数据上报次数统计
     * @param key
     * @return
     */
    @RequestMapping(value = "incr",method = RequestMethod.POST)
    @ResponseBody
    public MSG incr(@RequestParam(value = "key") String key){

        long delta = redisUtil.incr(key,1L);
        logger.info("cache,equipment reported data again,euqipmentId:" );

        MSG msg = new MSG();
        if (delta > 0){
            msg.setStatus("200");
            msg.setData(delta);
        }
        return msg;
    }
}
